//
//  BoardLayer.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class BoardLayer: CALayer {
  var pathId: String = ""
  override init() {
    super.init()
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
