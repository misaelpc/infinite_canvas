//
//  CenterView.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class CenterView: UIView {

  @IBOutlet weak var hostScrollView: UIScrollView?
  @IBOutlet weak var topConstraint: NSLayoutConstraint?
  @IBOutlet weak var bottomConstraint: NSLayoutConstraint?
  @IBOutlet weak var leftConstraint: NSLayoutConstraint?
  @IBOutlet weak var rightConstraint: NSLayoutConstraint?
  
  var delegate: CenterViewProtocol!
  
  private(set) var referenceCoordinates: (Int, Int) = (0, 0)
  private(set) var centreCoordinates: (Int, Int) = (Int.max, Int.max)
  private var arbitraryLargeOffset: CGFloat = 1000.0
  private var allocateBoards: [GridView] = []
  private(set) var observingScrollview: Bool = false
  
  deinit {
    if observingScrollview {
      hostScrollView?.removeObserver(self, forKeyPath: "contentOffset")
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    defineScrollableArea()
    centreOurReferenceView()
    allocateInitialBoards()
    observeScrollview()
  }
  
  private func defineScrollableArea() {
    guard let scrollview = hostScrollView else { return }
    arbitraryLargeOffset = scrollview.frame.size.width
    topConstraint?.constant = arbitraryLargeOffset
    bottomConstraint?.constant = arbitraryLargeOffset
    leftConstraint?.constant = arbitraryLargeOffset
    rightConstraint?.constant = arbitraryLargeOffset
    scrollview.delegate = self
    scrollview.layoutIfNeeded()
  }
  
  private func centreOurReferenceView() {
    guard let scrollview = hostScrollView else { return }
    let xOffset = arbitraryLargeOffset - ((scrollview.frame.size.width - self.frame.size.width) * 0.5)
    let yOffset = arbitraryLargeOffset - ((scrollview.frame.size.height - self.frame.size.height) * 0.5)
    scrollview.setContentOffset(CGPoint(x: xOffset, y: yOffset), animated: false)
  }
  
  private func allocateInitialBoards() {
    if let scrollview = hostScrollView {
      adjustGrid(for: scrollview)
    }
  }
  
  private func allocateBoard(at boardCoordinates: (Int, Int)) {
    guard gridExists(at: boardCoordinates) == false else { return }
    let board = GridView(frame: frameForTile(at: boardCoordinates), coordinates: boardCoordinates)
    board.isUserInteractionEnabled = true
    board.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
    allocateBoards.append(board)
    self.addSubview(board)
  }
  
  private func frameForTile(at coordinates: (Int, Int)) -> CGRect {
    guard let scrollview = hostScrollView else { return self.frame }
    let xIntOffset = coordinates.0 - referenceCoordinates.0
    let yIntOffset = coordinates.1 - referenceCoordinates.1
    let xOffset = self.bounds.size.width * 0.5 + (scrollview.bounds.size.width * (CGFloat(xIntOffset) - 0.5))
    let yOffset = self.bounds.size.height * 0.5 + (scrollview.bounds.size.height * (CGFloat(yIntOffset) - 0.5))
    return CGRect(x: xOffset, y: yOffset, width: scrollview.bounds.size.width, height: scrollview.bounds.size.height)
  }
  
  private func populateGridInBounds(lowerX: Int, upperX: Int, lowerY: Int, upperY: Int) {
    guard upperX > lowerX, upperY > lowerY else { return }
    var coordX = lowerX
    while coordX <= upperX {
      var coordY = lowerY
      while coordY <= upperY {
        allocateBoard(at: (coordX, coordY))
        coordY += 1
      }
      coordX += 1
    }
  }
  
  private func gridExists(at gridCoordinates: (Int, Int)) -> Bool {
    for grid in allocateBoards where grid.coordinates == gridCoordinates {
      return true
    }
    return false
  }
  
  private func clearGridOutsideBounds(lowerX: Int, upperX: Int, lowerY: Int, upperY: Int) {
    let gridsToProcess = allocateBoards
    for grid in gridsToProcess {
      let gridX = grid.coordinates.0
      let gridY = grid.coordinates.1
      if gridX < lowerX || gridX > upperX || gridY < lowerY || gridY > upperY {
        print("Deallocating grid tile: \(grid.coordinates)")
        grid.removeFromSuperview()
        if let index = allocateBoards.index(of: grid) {
          allocateBoards.remove(at: index)
        }
      }
    }
  }
  
  private func observeScrollview() {
    guard observingScrollview == false,
      let scrollview = hostScrollView
      else { return }
    scrollview.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
    scrollview.delegate = self
    observingScrollview = true
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    guard let scrollview = object as? UIScrollView else { return }
    adjustGrid(for: scrollview)
  }
  
  private func adjustGrid(for scrollview: UIScrollView) {
    let centre = computedCentreCoordinates(scrollview)
    guard centre != centreCoordinates else { return }
    self.centreCoordinates = centre
    guard let scrollView = hostScrollView, scrollView.bounds.size.width > 0 else { return }
    debugPrint("adjust grid")
    let xTilesRequired = Int(UIScreen.main.bounds.size.width / (scrollView.bounds.size.width * 0.9))
    let yTilesRequired = Int(UIScreen.main.bounds.size.height / (scrollView.bounds.size.height * 0.9))
    let lowerBoundX = centre.0 - xTilesRequired
    let upperBoundX = centre.0 + xTilesRequired
    let lowerBoundY = centre.1 - yTilesRequired
    let upperBoundY = centre.1 + yTilesRequired
    populateGridInBounds(lowerX: lowerBoundX, upperX: upperBoundX,
                         lowerY: lowerBoundY, upperY: upperBoundY)
    clearGridOutsideBounds(lowerX: lowerBoundX, upperX: upperBoundX,
                           lowerY: lowerBoundY, upperY: upperBoundY)
  }
  
  private func computedCentreCoordinates(_ scrollview: UIScrollView) -> (Int, Int) {
    guard scrollview.bounds.size.width > 0 else { return centreCoordinates }
    let contentOffset = scrollview.contentOffset
    let scrollviewSize = scrollview.frame.size
    let xOffset = -(self.center.x - (contentOffset.x + scrollviewSize.width * 0.5))
    let yOffset = -(self.center.y - (contentOffset.y + scrollviewSize.height * 0.5))
    let xIntOffset = Int((xOffset /  (hostScrollView?.bounds.size.width)!).rounded())
    let yIntOffset = Int((yOffset /  (hostScrollView?.bounds.size.height)!).rounded())
    return (xIntOffset + referenceCoordinates.0, yIntOffset + referenceCoordinates.1)
  }
  
}

extension CenterView: UIScrollViewDelegate {
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    guard decelerate == false else { return }
    self.readjustOffsets()
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    self.readjustOffsets()
  }
  
  private func readjustOffsets() {
    guard
      centreCoordinates != referenceCoordinates,
      let scrollview = hostScrollView,
      scrollview.bounds.size.width > 0,
      scrollview.bounds.size.height > 0
      else { return }
    let xOffset = CGFloat(centreCoordinates.0 - referenceCoordinates.0) * scrollview.bounds.size.width
    let yOffset = CGFloat(centreCoordinates.1 - referenceCoordinates.1) * scrollview.bounds.size.height
    referenceCoordinates = centreCoordinates
  
    for tile in allocateBoards {
      var frame = tile.frame
      frame.origin.x -= xOffset
      frame.origin.y -= yOffset
      tile.frame = frame
    }
    
    var newContentOffset = scrollview.contentOffset
    newContentOffset.x -= xOffset
    newContentOffset.y -= yOffset
    scrollview.setContentOffset(newContentOffset, animated: false)
    
    let centerOffset = CGPoint(x: newContentOffset.x / arbitraryLargeOffset, y: newContentOffset.y / arbitraryLargeOffset)
    debugPrint("Center offset \(centerOffset)")
    debugPrint("Center x \(newContentOffset.x)")
    debugPrint("Center y \(newContentOffset.y)")
    
    debugPrint("referenceCoordinates \(computedCentreCoordinates(scrollview))")
    delegate.scroll(didEndDragging: computedCentreCoordinates(scrollview), offset: (xOffset, yOffset))
  }
  
}
