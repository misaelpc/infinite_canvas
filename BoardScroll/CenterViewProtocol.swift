//
//  GridViewProtocol.swift
//  BoardScroll
//
//  Created by Alex Ech on 10/19/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

protocol CenterViewProtocol {
  func scroll(didEndDragging coordinates:  (Int, Int), offset: (CGFloat, CGFloat))
}
