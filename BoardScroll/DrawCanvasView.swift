//
//  DrawCanvasView.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class DrawCanvasView: UIImageView {
  
  var lastPoint = CGPoint.zero
  let pathStorage = PathStorage()
  var brushWidth: CGFloat = 2.0
  weak var delegate: freeHandDrawingDelegate?
  private let forceSensitivity: CGFloat = 4.0
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first {
      lastPoint = touch.location(in: self)
      let pencilPoint = PencilPoint(point: lastPoint, strokeWith: brushWidth)
      pathStorage.savePath(point: pencilPoint)
    }
  }
  
  func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
    UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
    let context = UIGraphicsGetCurrentContext()
    image?.draw(in: bounds)
    context!.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
    context!.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
    context!.setLineCap(.round)
    context!.setLineWidth(brushWidth)
    context!.setStrokeColor(UIColor.blue.cgColor)
    context!.setBlendMode(.normal)
    context!.strokePath()
    let pencilPoint = PencilPoint(point: lastPoint, strokeWith: brushWidth)
    pathStorage.savePath(point: pencilPoint)
    let partialImage = UIGraphicsGetImageFromCurrentImageContext()
    image = partialImage
    UIGraphicsEndImageContext()
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first {
      brushWidth = lineWidthForDrawing(touch: touch)
      let currentPoint = touch.location(in: self)
      drawLineFrom(fromPoint: lastPoint, toPoint: currentPoint)
      lastPoint = currentPoint
    }
  }

  private func lineWidthForDrawing(touch: UITouch) -> CGFloat {
    if touch.force > 0 {
      let lineWidth = touch.force * forceSensitivity
      return lineWidth
    }
    return brushWidth
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    image = nil
    let layer = FreeHandLayer()
    layer.frame = self.bounds
    layer.currentColor = UIColor.blue
    layer.layerPath = pathStorage.paths
    delegate?.canvas(didFinishDrawingInCanvas: layer, lastPoint: lastPoint)
    //self.layer.addSublayer(layer)
    //layer.setNeedsDisplay()
    pathStorage.paths.removeAll()
    //self.layoutIfNeeded()
  }

}
