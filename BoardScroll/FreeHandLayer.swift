//
//  FreeHandLayer.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class FreeHandLayer: BoardLayer {
  
  var layerPath: [PencilPoint] = []
  var red: CGFloat = 0.0
  var green: CGFloat = 0.0
  var blue: CGFloat = 0.0
  var currentColor: UIColor = UIColor.black
  var brushWidth: CGFloat = 2.0
  
  override init() {
    super.init()
    self.pathId = ""
    zPosition = 4
    debugPrint(layerPath)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(in ctx: CGContext) {
    var lastPoint = layerPath[0]
    for pencilPoint in layerPath {
      let path = UIBezierPath()
      path.move(to: lastPoint.point)
      path.addLine(to: pencilPoint.point)
      lastPoint = pencilPoint
      let shapeLayer = CAShapeLayer()
      shapeLayer.path = path.cgPath
      shapeLayer.strokeColor = currentColor.cgColor
      shapeLayer.lineWidth = pencilPoint.strokeWith
      shapeLayer.lineCap = CAShapeLayerLineCap(rawValue: "round")
      addSublayer(shapeLayer)
    }
  }
}

