//
//  FreehandLayerProtocol.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

protocol freeHandDrawingDelegate: class {
  func canvas(didFinishDrawingInCanvas layer: FreeHandLayer, lastPoint: CGPoint)
}
