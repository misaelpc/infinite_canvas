//
//  GridView.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class GridView: UIView {
  let verticalSpace: CGFloat = 20
  let horizontalSpace: CGFloat = 20
  let coordinates: (Int, Int)
  
  init(frame: CGRect, coordinates: (Int, Int)) {
    self.coordinates = coordinates
    super.init(frame: frame)
    addCoordinatesLabel()
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  override func draw(_ rect: CGRect) {
    backgroundColor = UIColor.white
    let path = UIBezierPath()
    let lines = numberOflines()
    for index in 1...lines {
      path.move(to: CGPoint(x: 10, y: CGFloat(index) * verticalSpace))
      path.addLine(to: CGPoint(x: frame.width, y: CGFloat(index) * verticalSpace))
    }
    path.lineWidth = 2
    let dashes: [CGFloat] = [0.001, path.lineWidth * horizontalSpace]
    path.setLineDash(dashes, count: dashes.count, phase: 0)
    path.lineCapStyle = CGLineCap.round
    UIColor.lightGray.setStroke()
    path.stroke()
  }
  
  private func addCoordinatesLabel() {
    let label = UILabel(frame: self.bounds)
    label.text = "\(coordinates)"
    label.font = UIFont.systemFont(ofSize: 24.0)
    label.textColor = UIColor.darkGray
    label.textAlignment = .center
    label.minimumScaleFactor = 0.5
    label.adjustsFontSizeToFitWidth = true
    self.addSubview(label)
  }
  
  func numberOflines() -> Int {
    let heigth = frame.height
    let lines = heigth / verticalSpace
    return Int(lines)
  }
  
}
