//
//  PathStorage.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//
import UIKit

class PathStorage: NSObject {
  
  var paths: [PencilPoint] = []
  
  func savePath(point: PencilPoint) {
    paths.append(point)
  }
}
