//
//  pencilPoint.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

struct PencilPoint {
  var point: CGPoint
  var strokeWith: CGFloat
}
