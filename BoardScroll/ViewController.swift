//
//  ViewController.swift
//  BoardScroll
//
//  Created by Misael Pérez Chamorro on 10/18/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet weak var centerView: CenterView!
  @IBOutlet weak var canvasView: DrawCanvasView!
  
  private var currentCoordinates: (Int, Int) = (0, 0)
  private var currentOffset: (CGFloat, CGFloat) = (0, 0)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    canvasView.delegate = self
    centerView.delegate = self
  }
  
  @IBAction func selectButtonWasToucedUpInside(_ sender: UIButton) {
    if sender.isSelected {
      canvasView.isUserInteractionEnabled = false
      sender.isSelected = false
    } else {
      canvasView.isUserInteractionEnabled = true
      sender.isSelected = true
    }
  }
}

extension ViewController: freeHandDrawingDelegate {
  func canvas(didFinishDrawingInCanvas layer: FreeHandLayer, lastPoint: CGPoint) {
    layer.position = lastPoint
    centerView.layer.addSublayer(layer)
    layer.setNeedsDisplay()
  }
}

extension ViewController: CenterViewProtocol {
  func scroll(didEndDragging coordinates: (Int, Int), offset: (CGFloat, CGFloat)) {
    currentCoordinates = coordinates
    currentOffset = offset
  }
}

